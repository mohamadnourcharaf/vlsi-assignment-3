

def scheduleWithFDLS(nodes,timeConstraint,M):

    # Loop in time steps
    c_step = 1
    while c_step <= timeConstraint:

        timeConstraint = handleOperator("*",timeConstraint,nodes,c_step,M[0])
        timeConstraint = handleOperator("ALU",timeConstraint,nodes,c_step,M[1])

        c_step += 1

    # Sort
    nodes.sort(key=lambda item: item.fdlsTime, reverse=False)


def handleOperator(operator,timeConstraint,nodes,c_step,numberOfFuctionalUnits):

    for node in nodes:
        node.mobility = 0 if node.isFixed else node.alapTime - node.asapTime

    readyOperations = getReadyOperations(operator,c_step, nodes)

    while len(readyOperations) > numberOfFuctionalUnits:

        if allOperationsOnCriticalPath(readyOperations):
            timeConstraint += 1
            for node in nodes:
                node.alapTime += 1
                node.mobility = 0 if node.isFixed else node.alapTime - node.asapTime

        # Calculate Min Forces for possible Deferrals
        calculateMinForces(c_step, readyOperations)
        # Get ready operation with lowest force
        readyOperations.sort(key=lambda item: item.minForce, reverse=False)
        readyOperation = None
        for node in readyOperations:
            if node.mobility != 0:  # Not on critical path
                readyOperation = node
                break
        # Defer Operation
        if readyOperation is None:
            print("ERROR: ReadyOperation must not be None")
            exit(0)
        readyOperation.asapTime = readyOperation.min_deferred_c_step
        deferOperation(readyOperation)
        # Remove Operation
        readyOperations.remove(readyOperation)

    for readyOperation in readyOperations:
        readyOperation.fdlsTime = c_step
        readyOperation.isFixed = True

    return timeConstraint


def deferOperation(node):

    if len(node.outEdges) == 0:
        return

    for edge in node.outEdges:
        if edge.endNode.asapTime < (node.asapTime + 1):
            edge.endNode.asapTime = node.asapTime + 1
        deferOperation(edge.endNode)


def calculateMinForces(c_step,nodes):

    for node in nodes:
        min_deferred_c_step = c_step + 1
        minForce = calculateForce(min_deferred_c_step, nodes, node)
        for i in range(min_deferred_c_step, node.alapTime + 1):
            force = calculateForce(i,nodes,node)
            force += calculateIndirectForces(i,nodes,node,0)
            if force < minForce:
                min_deferred_c_step = i
                minForce = force
        node.min_deferred_c_step = min_deferred_c_step
        node.minForce = minForce


def calculateIndirectForces(i,nodes,node,sum):

    if len(node.outEdges) == 0:
        return 0

    for edge in node.outEdges:
        if edge.endNode.asapTime < (i + 1):
            sum += calculateForce(i + 1,nodes,edge.endNode) + calculateIndirectForces(i + 1,nodes,edge.endNode,sum)

    return sum


def calculateForce(c_step,nodes,node):

    oldProbability = 1/(node.mobility + 1)

    force = 0
    for i in range(node.asapTime,node.alapTime + 1):
        DG = calculateDG(i,nodes)
        newProbability = 1 if i == c_step else 0
        force += (DG * (newProbability - oldProbability))

    return force


def calculateDG(c_step,nodes):

    DG = 0
    for node in nodes:
        if node.asapTime <= c_step <= node.alapTime:
            probability = 1 / (node.mobility + 1)
            DG += probability
    return DG


def allOperationsOnCriticalPath(nodes):
    allCritical = True
    for node in nodes:
        if node.mobility != 0:
            allCritical = False
    return allCritical


def getReadyOperations(operator,c_step, nodes):
    readyNodes = []
    for node in nodes:
        if node.asapTime <= c_step <= node.alapTime and not node.isFixed:
            if (operator == "*" and node.operator == "*") or (operator != "*" and node.operator != "*"):
                readyNodes.append(node)
    return readyNodes


def printFDLSResult(nodes):
    # Print Result
    print("FDLS")
    for i in range(0,len(nodes)):
        node = nodes[i]
        print("Node with output " + node.output + " has FDLS time: " + str(node.fdlsTime))