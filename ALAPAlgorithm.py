

def scheduleWithALAP(nodes, timeConstraint):

    tempNodes = []

    for node in nodes:

        # Check if has successor
        if len(node.outEdges) == 0:
            node.alapTime = timeConstraint
        else:
            node.alapTime = 0 # Meaning not scheduled
            tempNodes.append(node)

    while len(tempNodes) != 0:

        for node in tempNodes:
            if check_if_all_succ_scheduled(node):
                succNodes = []
                for edge in node.outEdges:
                    if edge.endNode is not None:
                        succNodes.append(edge.endNode)

                minNode = min(succNodes, key=lambda item: item.alapTime)
                node.alapTime = minNode.alapTime - 1

                tempNodes.remove(node)

    # Sort
    nodes.sort(key=lambda item: item.alapTime, reverse=False)


def check_if_all_succ_scheduled(node):

    allScheduled = True
    for edge in node.outEdges:
        if not all_succ_scheduled(edge.endNode):
            allScheduled = False
            break

    return allScheduled


def all_succ_scheduled(node):

    if node is None:
        return True

    if node.alapTime == 0:
        return False

    allScheduled = True
    for edge in node.outEdges:
        if not all_succ_scheduled(edge.endNode):
            allScheduled = False
            break

    return allScheduled


def printALAPResult(nodes):
    # Print Result
    print("ALAP")
    for i in range(0,len(nodes)):
        node = nodes[i]
        print("Node with output " + node.output + " has ALAP time: " + str(node.alapTime))