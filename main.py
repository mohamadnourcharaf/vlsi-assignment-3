import Parameters
import DataManager
import ASAPAlgorithm
import ALAPAlgorithm
import FDLSAlgorithm
import ILPTimeConstrainedScheduling
import ILPResourceConstrainedScheduling


def start():

    # ILP Time Constrained Scheduling
    ilpTimeConstrainedScheduling()

    # ILP Resource Constrained Scheduling
    ilpResourceConstrainedScheduling()


def ilpResourceConstrainedScheduling():

    # Get Nodes
    nodes = DataManager.getDFG(Parameters.fileName, Parameters.separator)

    # ASAP
    ASAPAlgorithm.scheduleWithASAP(nodes)
    ASAPAlgorithm.printASAPResult(nodes)

    # ALAP
    timeConstraint = nodes[-1].asapTime
    ALAPAlgorithm.scheduleWithALAP(nodes, timeConstraint)
    ALAPAlgorithm.printALAPResult(nodes)

    # FDLS
    FDLSAlgorithm.scheduleWithFDLS(nodes, timeConstraint, Parameters.M)
    FDLSAlgorithm.printFDLSResult(nodes)

    # ILP Resource Constrained Scheduling
    upperBound = nodes[-1].fdlsTime
    ILPResourceConstrainedScheduling.ilpResourceConstrainedScheduling(nodes, upperBound, Parameters.M)


def ilpTimeConstrainedScheduling():

    # Get Nodes
    nodes = DataManager.getDFG(Parameters.fileName, Parameters.separator)

    # ASAP
    ASAPAlgorithm.scheduleWithASAP(nodes)
    ASAPAlgorithm.printASAPResult(nodes)

    # ALAP
    timeConstraint = Parameters.s #nodes[-1].asapTime
    ALAPAlgorithm.scheduleWithALAP(nodes,timeConstraint)
    ALAPAlgorithm.printALAPResult(nodes)

    # ILP Time Constrained Scheduling
    ILPTimeConstrainedScheduling.ilpTimeConstrainedScheduling(nodes,Parameters.s,Parameters.c)


start()
