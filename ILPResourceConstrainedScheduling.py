from mip import Model, xsum, maximize, minimize, BINARY, INTEGER


def ilpResourceConstrainedScheduling(nodes, upperBound, M):

    # Model
    model = Model("ResourceConstrainedScheduling")

    # Variables
    # Add variables for each node
    x = []
    for i in range(0, len(nodes)):
        variables = []
        for j in range(0, upperBound):
            variable = model.add_var(var_type=BINARY)
            variables.append(variable)
        x.append(variables)

    # Add c_step variable
    c_step = model.add_var(var_type=INTEGER)

    # Objective Function
    # The objective function in (1.1) states that we are going to minimize the total number of control steps
    model.objective = minimize(c_step)

    # Constraints
    # Constraint (2.1) states that no schedule should have a control step containing more than Mtk function units of type tk
    for j in range(0, upperBound):
        timeStep = j + 1
        for k in range(0, len(M)):
            possibleNodesIndecies = []
            for i in range(0, len(nodes)):
                node = nodes[i]
                if (node.asapTime <= timeStep <= node.alapTime) and node.getOperatorType() == k:
                    possibleNodesIndecies.append(i)

            model += (xsum(x[i][j] for i in possibleNodesIndecies)) <= M[k]

    # Constraints (3) and (4) are the same as those in time-constrained scheduling
    for i in range(0, len(nodes)):
        node = nodes[i]
        model += xsum(x[i][j] for j in range(node.asapTime - 1, node.alapTime)) == 1

    # Constraints (3) and (4) are the same as those in time-constrained scheduling
    for k in range(0, len(nodes)):
        node = nodes[k]
        for edge in node.inEdges:
            parentNode = edge.startNode
            if parentNode is not None:
                i = nodes.index(parentNode)

                model += (xsum(j * x[i][j] for j in range(parentNode.asapTime - 1, parentNode.alapTime)) - xsum(
                    j * x[k][j] for j in range(node.asapTime - 1, node.alapTime))) <= -1

    # No operations should be scheduled after Cstep, as described in constraint (5)
    for i in range(0,len(nodes)):
        node = nodes[i]
        if len(node.outEdges) == 0:
            model += (xsum(j * x[i][j] for j in range(node.asapTime - 1, node.alapTime)) - c_step) <= 0

    # Optimize
    model.optimize()

    # Print Results
    print("ILP Resource Constrained Scheduling")
    for i in range(0, len(x)):
        node = nodes[i]
        for j in range(0, len(x[i])):
            value = x[i][j].x
            if value is not None and value >= 0.99:
                print("Node with output " + node.output + " has ILP Resource Constrained Scheduling time: " + str(
                    j + 1) + " " + str(node.operator))

    print("\nTime")
    print("Number of Steps: " + str(c_step.x + 1))
