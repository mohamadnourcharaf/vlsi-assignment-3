

def scheduleWithASAP(nodes):

    tempNodes = []

    for node in nodes:

        hasPred = False
        for edge in node.inEdges:
            if edge.startNode is not None:
                hasPred = True

        if hasPred:
            node.asapTime = 0 # Meaning not scheduled
            tempNodes.append(node)
        else:
            node.asapTime = 1

    while len(tempNodes) != 0:

        for node in tempNodes:
            if check_if_all_pred_scheduled(node):
                predNodes = []
                for edge in node.inEdges:
                    if edge.startNode is not None:
                        predNodes.append(edge.startNode)

                maxNode = max(predNodes, key=lambda item: item.asapTime)
                node.asapTime = maxNode.asapTime + 1

                tempNodes.remove(node)

    # Sort
    nodes.sort(key=lambda item: item.asapTime, reverse=False)


def check_if_all_pred_scheduled(node):

    allScheduled = True
    for edge in node.inEdges:
        if not all_pred_scheduled(edge.startNode):
            allScheduled = False
            break

    return allScheduled


def all_pred_scheduled(node):

    if node is None:
        return True

    if node.asapTime == 0:
        return False

    allScheduled = True
    for edge in node.inEdges:
        if not all_pred_scheduled(edge.startNode):
            allScheduled = False
            break

    return allScheduled


def printASAPResult(nodes):
    # Print Result
    print("ASAP")
    for i in range(0,len(nodes)):
        node = nodes[i]
        print("Node with output " + node.output + " has ASAP time: " + str(node.asapTime))