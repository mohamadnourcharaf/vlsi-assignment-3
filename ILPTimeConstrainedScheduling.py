from mip import Model, xsum, maximize, minimize, BINARY, INTEGER


def ilpTimeConstrainedScheduling(nodes, s, c):

    # Model
    model = Model("TimeConstrainedScheduling")

    # Variables
    # Add variables for each node
    x = []
    for i in range(0, len(nodes)):
        variables = []
        for j in range(0, s):
            variable = model.add_var(var_type=BINARY)
            variables.append(variable)
        x.append(variables)

    # Add variables for each resource type
    M = []
    for i in range(0, len(c)):
        variable = model.add_var(var_type=INTEGER)
        M.append(variable)

    # Objective Function
    # The objective function in (1) states that we are going to minimize the total cost of function units
    model.objective = minimize(xsum(c[k] * M[k] for k in range(0, len(c))))

    # Constraints
    # Constraint (2) states that no schedule should have a control step containing more than M function units of type tk
    for j in range(0, s):
        timeStep = j + 1
        for k in range(0, len(c)):
            possibleNodesIndecies = []
            for i in range(0, len(nodes)):
                node = nodes[i]
                if (node.asapTime <= timeStep <= node.alapTime) and node.getOperatorType() == k:
                    possibleNodesIndecies.append(i)

            model += (xsum(x[i][j] for i in possibleNodesIndecies)) - M[k] <= 0

    # It is clear that oi, can only be scheduled into a step between Si, and Li, which is reflected in (3)
    for i in range(0, len(nodes)):
        node = nodes[i]
        model += xsum(x[i][j] for j in range(node.asapTime - 1, node.alapTime)) == 1

    # Constraint (4) ensures that the precedence relations of the data flow graph (DFG) will be preserved
    for k in range(0, len(nodes)):
        node = nodes[k]
        for edge in node.inEdges:
            parentNode = edge.startNode
            if parentNode is not None:
                i = nodes.index(parentNode)

                model += (xsum(j * x[i][j] for j in range(parentNode.asapTime - 1, parentNode.alapTime)) - xsum(
                    j * x[k][j] for j in range(node.asapTime - 1, node.alapTime))) <= -1

    # Optimize
    model.optimize()

    # Print Results
    print("ILP Time Constrained Scheduling")
    for i in range(0, len(x)):
        node = nodes[i]
        for j in range(0, len(x[i])):
            value = x[i][j].x
            if value is not None and value >= 0.99:
                print("Node with output " + node.output + " has ILP Time Constrained Scheduling time: " + str(
                    j + 1) + " " + str(node.operator))

    print("\nResources")
    print("Number of Multipliers: " + str(M[0].x))
    print("Number of ALUs: " + str(M[1].x))
