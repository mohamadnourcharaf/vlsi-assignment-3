
diffeqFileName = "diffeq.dfg"
ellipticFileName = "elliptic.dfg"
separator = " "

fileName = diffeqFileName

# Time Constrained Scheduling Parameters
s = 7 # Note: For diffeq, use 7 or above. For elliptic, use 15 or above
c = [5,1] # Cost for 1: Multipliers, 2: ALUs

# Resource Constrained Scheduling Parameters
M = [2,2] # Max number of 1: Multipliers, 2: ALUs